<?php namespace Web\Base\Components;

use Cms\Classes\ComponentBase;

class BaseContact extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseContact Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onSave()
    {
        $contact          = new \Andalan\Contact\Models\Contact;
        $contact->name    = post('name');
        $contact->phone   = post('phone');
        $contact->subject = post('subject');
        $contact->body    = post('body');
        $contact->save();

        \Flash::success('Pesan berhasil dikirim');
        return \Redirect::refresh();
    }
}
