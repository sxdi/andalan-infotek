<?php namespace Andalan\Core;

use Backend;
use System\Classes\PluginBase;

/**
 * Core Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Core',
            'description' => 'No description provided yet...',
            'author'      => 'Andalan',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Andalan\Core\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'andalan.core.some_permission' => [
                'tab' => 'Core',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // return []; // Remove this line to activate

        return [
            'core' => [
                'label'       => 'Andalan',
                'url'         => Backend::url('andalan/core/portfolios'),
                'icon'        => 'icon-leaf',
                'permissions' => ['andalan.core.*'],
                'order'       => 500,
                'sideMenu'    => [
                    'portfolios' => [
                        'label'       => 'Portfolio',
                        'url'         => Backend::url('andalan/core/portfolios'),
                        'icon'        => 'icon-briefcase',
                        'permissions' => ['andalan.core.*'],
                        'order'       => 500,
                    ],
                    'reviews' => [
                        'label'       => 'Testimoni',
                        'url'         => Backend::url('andalan/core/reviews'),
                        'icon'        => 'icon-comments',
                        'permissions' => ['andalan.core.*'],
                        'order'       => 500,
                    ],
                    'clients' => [
                        'label'       => 'Pelanggan',
                        'url'         => Backend::url('andalan/core/clients'),
                        'icon'        => 'icon-user',
                        'permissions' => ['andalan.core.*'],
                        'order'       => 500,
                    ],
                    'contacts' => [
                        'label'       => 'Inbox',
                        'url'         => Backend::url('andalan/core/contacts'),
                        'icon'        => 'icon-inbox',
                        'permissions' => ['andalan.core.*'],
                        'order'       => 500,
                    ],
                ]
            ],
        ];
    }
}
