<?php namespace Andalan\Portfolio;

use Backend;
use System\Classes\PluginBase;

/**
 * Portfolio Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Portfolio',
            'description' => 'No description provided yet...',
            'author'      => 'Andalan',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Andalan\Portfolio\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'andalan.portfolio.some_permission' => [
                'tab' => 'Portfolio',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'portfolio' => [
                'label'       => 'Portfolio',
                'url'         => Backend::url('andalan/portfolio/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['andalan.portfolio.*'],
                'order'       => 500,
            ],
        ];
    }
}
