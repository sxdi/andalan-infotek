<?php namespace Andalan\Client;

use Backend;
use System\Classes\PluginBase;

/**
 * Client Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Client',
            'description' => 'No description provided yet...',
            'author'      => 'Andalan',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Andalan\Client\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'andalan.client.some_permission' => [
                'tab' => 'Client',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'client' => [
                'label'       => 'Client',
                'url'         => Backend::url('andalan/client/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['andalan.client.*'],
                'order'       => 500,
            ],
        ];
    }
}
