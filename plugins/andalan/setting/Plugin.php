<?php namespace Andalan\Setting;

use Backend;
use System\Classes\PluginBase;

/**
 * Setting Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Setting',
            'description' => 'No description provided yet...',
            'author'      => 'Andalan',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        \Event::listen('cms.page.beforeDisplay', function($controller, $page, $url) {
            $controller->vars['settings'] = [
                'site'       => \Andalan\Setting\Models\Site::instance(),
                'page'       => \Andalan\Setting\Models\Page::instance(),
                'appearance' => \Andalan\Setting\Models\Appearance::instance(),
            ];
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Andalan\Setting\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'andalan.setting.some_permission' => [
                'tab' => 'Setting',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'setting' => [
                'label'       => 'Setting',
                'url'         => Backend::url('andalan/setting/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['andalan.setting.*'],
                'order'       => 500,
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'sites' => [
                'label'       => 'Site',
                'description' => 'Manage available web site settings',
                'category'    => 'Web',
                'icon'        => 'icon-globe',
                'class'       => 'Andalan\Setting\Models\Site',
                'order'       => 1,
            ],
            'appearances' => [
                'label'       => 'Appearance',
                'description' => 'Manage available web appearance settings',
                'category'    => 'Web',
                'icon'        => 'icon-image',
                'class'       => 'Andalan\Setting\Models\Appearance',
                'order'       => 2,
            ],
            'pages' => [
                'label'       => 'Page',
                'description' => 'Manage available web page settings',
                'category'    => 'Web',
                'icon'        => 'icon-file',
                'class'       => 'Andalan\Setting\Models\Page',
                'order'       => 3,
            ],
        ];
    }
}
