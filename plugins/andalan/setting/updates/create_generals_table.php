<?php namespace Andalan\Setting\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateGeneralsTable extends Migration
{
    public function up()
    {
        Schema::create('andalan_setting_generals', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('andalan_setting_generals');
    }
}
